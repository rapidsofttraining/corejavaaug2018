package com.rapidsoft.neelu;

public class ReverseString {

	public static void main(String[] args) {

		String name = "Neelu";
		reverse(name);
	}

	public static void reverse(String name) {

		// System.out.println(name.length());
		int k = name.length();
		k = k-1;
		String reversename = "";
		for (int i = k; i >= 0; i--) {
			
			char b = name.charAt(i);
			reversename = reversename + b;
			
		}
		System.out.println(reversename);
	}

}
