package com.rapidsoft.neelu;

public class TheDog extends TheAnimal {
	
	public void Walk()
	{
		System.out.println("Walking from TheDog");
	}
	
	public void Walk(String s)
	{
		System.out.println("Walking from TheDog " + s);
	}
	
}
