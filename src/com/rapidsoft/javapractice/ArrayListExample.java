package com.rapidsoft.javapractice;

import java.util.ArrayList;

public class ArrayListExample {
	
	
	
	// List of Car Objects
	public static void main(String[] args) {
		
		ArrayList<Car> ListOfCars = new ArrayList<Car>();
		
		
		
		Car passat = new Car();
		//passat.start();
		passat.setName("vw passat");
		passat.setColor("Blue");
		passat.setSeatingCapacity("5");
		ListOfCars.add(passat);

		Car hondaaccord = new Car();
		//hondaaccord.start();
		hondaaccord.setName("honda accord");
		hondaaccord.setColor("Blue");
		hondaaccord.setSeatingCapacity("4");


		ListOfCars.add(hondaaccord);

	
		Car gx470 = new Car();
		//gx470.start();
		gx470.setName("lexus gx 470");
		gx470.setColor("Blue");
		gx470.setSeatingCapacity("8");

		ListOfCars.add(gx470);

//		for (int i=0; i < ListOfCars.size(); i++)
//		{
//			Car currentCar = ListOfCars.get(i);
//		}
		
		
      // enhanced forloop no initialiation rqured and no length requre no increment rq
		
		
		
		
		for(Car currentCar:ListOfCars)
		{
			
			String nameofcar = currentCar.getName();
			String colorofcar = currentCar.getColor();
			String seatCapacity = currentCar.getSeatingCapacity();


			System.out.println("name of the car is " + nameofcar + " and color of the car is "  + colorofcar + "with seating capacity of  " + seatCapacity);
			
			
		}
		
		
		
		
		
		
		
		
		
		
		
	}
	

	
	
	
	
	
	
	
	

}
