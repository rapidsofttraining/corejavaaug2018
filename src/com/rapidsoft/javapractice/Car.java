package com.rapidsoft.javapractice;

public class Car extends Vehicle {
	
	private String carname; 
	private String carcolor; 
	private String seatingCapacity;
	
	public String getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(String seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

	public void start()
	{
		System.out.println("in start of car");
	}
	
	public void start(String s)
	{
		System.out.println("in start of car " + s);
	}
	
	public void setName(String carperu )
	{
		carname = carperu;
	}
	
	public String getName()
	{
		return carname;
	}
	
	
	public void setColor(String color )
	{
		carcolor = color;
	}
	
	public String getColor()
	{
		return carcolor;
	}
	
	
}
