package com.rapidsoft.javapractice;

public class StringHandling {

	public static void main(String[] args) {

		String s = "RapidSoft Corp";

		int length = s.length();
		System.out.println(length);

		System.out.println(s.substring(2, 4));

		System.out.println(s.concat("Limited"));

		System.out.println(s);
		System.out.println(s.equals("RapidSoft Corp1"));

		System.out.println(s.equals("RapidSoft Corp"));

		System.out.println(s.replace("Soft", "Systems"));

	}

}
