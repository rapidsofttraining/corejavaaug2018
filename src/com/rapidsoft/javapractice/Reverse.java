package com.rapidsoft.javapractice;

public class Reverse {
	

	public static void main(String[] args) {
		 
		        System.out.println(reverseMe("Mikkilineni"));
		    }
		    static String reverseMe(String s){
		        StringBuffer sb=new StringBuffer();
		        for(int i=s.length()-1;i>=0;--i){
		            sb.append(s.charAt(i));
		        }
		        return sb.toString();
		    }

	}


