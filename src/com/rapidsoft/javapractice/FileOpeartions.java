package com.rapidsoft.javapractice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import com.rapidsoft.javapractice.Car;

public class FileOpeartions {

	public static void main(String[] args) {

		File myProfile = new File("myprop.txt");
        // how to append to the current file instead of new everytime?
		try {
			PrintWriter pw = new PrintWriter(myProfile);
			
			//pw.write("hello this is first sentence" + "\r\n");
			pw.write("hello this is first sentence" + "\r\n");
			// pw.write("hello my text" + "\r\n");
			// pw.write("hello other text" + "\r\n");
			// pw.write("hello other one more" + "\r\n");

			pw.close();

		} catch (FileNotFoundException e) {

			System.out.println("Cannot find the file");
		}
		
		
		

		try {
			
			Scanner reader = new Scanner(myProfile);
			if (reader.hasNextLine())
				System.out.println(reader.nextLine());
			if (reader.hasNextLine())
				System.out.println(reader.nextLine());
			if (reader.hasNextLine())
				System.out.println(reader.nextLine());
			if (reader.hasNextLine())
				System.out.println(reader.nextLine());
			
			

			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
